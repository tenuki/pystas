# README #

Pystas allows you get and track elemental function call statistics by just decorating the functions you're interested in!

    from pystas import logpista, logwargs, logw1arg

    @logpista
    def a_function_with_strange_behavior(bla):
        ...    
    
    @logw1arg
    def funcbyargs(arg1):
        ...
    
    @logwargs
    def funcbyallargs(arg1, arg2, arg3=None):
        ...
    
    class SomeClass:
        @logpista
        def some_interesting_method(self):
            ...


with that, you are ready to go. Run: `python -m pystas` in the same place you ran your program (after you did), and you'll get something like:

    last execution is: #7 started: 2017-09-07 13:27:56.659000 complete dump:
    #7| 0.03s. __main__:a_function_with_strange_behavior [RuntimeError]
    #7| 0.01s. __main__:a_function_with_strange_behavior
    #7| 0.01s. __main__:a_function_with_strange_behavior
    #7| 0.13s. __main__:funcbyallargs ((3, '3', 3), {})
    #7| 0.13s. __main__:funcbyallargs ((2, '2', 3), {})
    #7| 0.12s. __main__:funcbyallargs ((1, '1', 4), {})
    #7| 0.11s. __main__:funcbyallargs ((0, '0', 2), {})
    #7| 0.01s. __main__:funcbyargs (4)
    #7| 0.01s. __main__:funcbyargs (4)
    #7| 0.01s. __main__:funcbyargs (4)
    #7| 0.01s. __main__:funcbyargs (4)
    #7| 0.01s. __main__:funcbyargs (4)
    #7| 0.01s. __main__:funcbyargs (2)
    #7| 0.01s. __main__:funcbyargs (2)
    #7| 0.01s. __main__:funcbyargs (2)
    #7| 0.01s. __main__:funcbyargs (2)
    #7| 0.02s. __main__:funcbyargs (2)
    
    all-history
    calls   excp.   t.total   media                    function
    ----------------------------------------------------------------------------
       35       0     0.36s   0.01s   __main__:funcbyargs (2)
       35       0     0.36s   0.01s   __main__:funcbyargs (4)
        2       0     0.22s   0.11s   __main__:funcbyallargs ((0, '0', 2), {})
        2       0     0.24s   0.12s   __main__:funcbyallargs ((1, '1', 0), {})
        2       0     0.26s   0.13s   __main__:funcbyallargs ((2, '2', 4), {})
        1       0     0.11s   0.11s   __main__:funcbyallargs ((3, '3', 1), {})
       29       2     0.51s   0.02s   __main__:a_function_with_strange_behavior
        6       0     8.85s   1.48s   __main__:SomeClass.some_interesting_method
        1       0     0.12s   0.12s   __main__:funcbyallargs ((0, '0', 4), {})
        2       0     0.23s   0.11s   __main__:funcbyallargs ((1, '1', 4), {})
        2       0     0.26s   0.13s   __main__:funcbyallargs ((2, '2', 3), {})
        1       0     0.11s   0.11s   __main__:funcbyallargs ((3, '3', 2), {})
        1       0     0.11s   0.11s   __main__:funcbyallargs ((0, '0', 3), {})
        2       0     0.25s   0.12s   __main__:funcbyallargs ((1, '1', 1), {})
        1       0     0.11s   0.11s   __main__:funcbyallargs ((2, '2', 1), {})
        3       0     0.36s   0.12s   __main__:funcbyallargs ((3, '3', 3), {})
        2       0     0.22s   0.11s   __main__:funcbyallargs ((0, '0', 1), {})
        1       0     0.11s   0.11s   __main__:funcbyallargs ((2, '2', 0), {})
        1       0     0.11s   0.11s   __main__:funcbyallargs ((3, '3', 0), {})
    
    last-exec
    calls   excp.   t.total   media                   function
    ---------------------------------------------------------------------------
        5       0     0.06s   0.01s   __main__:funcbyargs (2)
        5       0     0.05s   0.01s   __main__:funcbyargs (4)
        1       0     0.11s   0.11s   __main__:funcbyallargs ((0, '0', 2), {})
        3       1     0.06s   0.02s   __main__:a_function_with_strange_behavior
        1       0     0.12s   0.12s   __main__:funcbyallargs ((1, '1', 4), {})
        1       0     0.13s   0.13s   __main__:funcbyallargs ((2, '2', 3), {})
        1       0     0.13s   0.13s   __main__:funcbyallargs ((3, '3', 3), {})
