import sys
import unittest

sys.argv.append('-pystasdb')
sys.argv.append(":memory:")

import pystas


# -- imports for test fragments ------------------------------------------------
import time
import pystas
from pystas import logpista
import traceback


class PystasTestsBase(unittest.TestCase):
    def setUp(self):
        pystas.init()

    def tearDown(self):
        for table in [pystas.Function, pystas.Src, pystas.Execution,
                      pystas.Errors, pystas.ProgExecution]:
            try:
                pystas.DB.drop_table(table)
            except pystas.OperationalError:
                raise

    def assertByMod(self, member, identifier, msg=None):
        return self.assertIn(member, [x+identifier for x in ['tests', '__main__'
                                                             ]])


class PystasFunctionTest(PystasTestsBase):
    def test_simple_function(self):
        exec("""
@logpista
def simple_function():
    time.sleep(0.1)
simple_function()
""")
        self.assertEqual(pystas.Function.select().count(), 1)
        self.assertEqual(pystas.Execution.select().count(), 1)
        self.assertEqual(pystas.Errors.select().count(), 0)
        self.assertByMod(str(pystas.Function.get()), ':simple_function')


if sys.version_info[0] == 2:
    CLASS_TYPES = OLDSTYLE, NEWSTYLE = 'OldStyle', 'NewStyle'
else:
    OLDSTYLE = 'AClass'
    CLASS_TYPES = (OLDSTYLE,)

for klass in CLASS_TYPES:
    class ClassTests(PystasTestsBase):
        ClassDef = staticmethod(
            lambda klass: klass if klass == OLDSTYLE else NEWSTYLE+'(object)')

        def test_simple_method(self):
            exec("""
class %s:
    @logpista
    def simple_method(self):
        time.sleep(0.1)
%s().simple_method()
            """ % (self.ClassDef(klass), klass))
            self.assertEqual(pystas.Function.select().count(), 1)
            self.assertEqual(pystas.Execution.select().count(), 1)
            self.assertEqual(pystas.Errors.select().count(), 0)
            self.assertByMod(str(pystas.Function.get()),
                             ':' + klass + '.simple_method')

        def test_simple_method_twice(self):
            exec("""
class %s:
    @logpista
    def simple_method(self):
        time.sleep(0.1)
%s().simple_method()
%s().simple_method()
        """ % (self.ClassDef(klass), klass, klass))
            self.assertEqual(pystas.Function.select().count(), 1)
            self.assertEqual(pystas.Execution.select().count(), 2)
            self.assertEqual(pystas.Errors.select().count(), 0)
            self.assertByMod(str(pystas.Function.get()),
                             ':' + klass + '.simple_method')

        def test_class_method(self):
            exec("""
class %s:
    @classmethod
    @logpista
    def simple_method(cls):
        time.sleep(0.1)
%s().simple_method()
%s.simple_method()
    """ % (self.ClassDef(klass), klass, klass))
            self.assertEqual(pystas.Function.select().count(), 1)
            self.assertEqual(pystas.Execution.select().count(), 2)
            self.assertEqual(pystas.Errors.select().count(), 0)
            self.assertByMod(str(pystas.Function.get()),
                             ':'+klass+'.simple_method')

        def test_error_one_multitimes(self):
            exec("""
class %s:
    @logpista
    def method_raises(self):
        raise Exception("asdf")

for _ in range(5):
    try:
        %s().method_raises()
    except:
        pass
    """ % (self.ClassDef(klass), klass))
            self.assertEqual(pystas.Function.select().count(), 1)
            self.assertEqual(pystas.Execution.select().count(), 5)
            self.assertEqual(pystas.Errors.select().count(), 1)
            self.assertByMod(str(pystas.Function.get()),
                             ':'+klass+'.method_raises')

        def test_error_multiple(self):
            exec("""
class %s:
    @logpista
    def method_raises(self, x):
        raise Exception(str(x))

for x in range(5):
    try:
        %s().method_raises(x)
    except:
        pass
    """ % (self.ClassDef(klass), klass))
            self.assertEqual(pystas.Function.select().count(), 1)
            self.assertEqual(pystas.Execution.select().count(), 5)
            self.assertEqual(pystas.Errors.select().count(), 5)
            self.assertByMod(str(pystas.Function.get()),
                             ':'+klass+'.method_raises')

        def test_static_method(self):
            exec("""
class %s:
    @staticmethod
    @logpista
    def static_method():
        time.sleep(0.1)
%s().static_method()
%s.static_method()
    """ % (self.ClassDef(klass), klass, klass))
            self.assertEqual(pystas.Function.select().count(), 1)
            self.assertEqual(pystas.Execution.select().count(), 2)
            self.assertEqual(pystas.Errors.select().count(), 0)
            self.assertByMod(str(pystas.Function.get()),
                             ':'+klass+'.static_method')

    exec(klass+'=ClassTests;'+klass+'.__name__=%r' % klass)
del ClassTests


if __name__ == "__main__":
    unittest.main()
